#pragma once
#include "Floor.h"
#include "Panel.h"
#include "TextContainer.h"
#include "InputReader.h"

class Scene :
	public Object
{
public:
	Scene();
	~Scene();
	
	virtual void					Draw();
	virtual void					Update(float dtime);

	void							TreatInput(float dtime);
	void							ComputeMouseMovementAngle();
	TextContainer*					m_textContainer;
private:

	void							Init();
	void							InitMenues();

	InputReader*					m_input;
	Vector2f						m_oldMousePos;

	Floor*							m_floor;
	Panel*							m_panel;
	std::vector<TextContainer*>		m_menuTexts;

	bool							m_paused;

};

extern Scene* g_scene;

