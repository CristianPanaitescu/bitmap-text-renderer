#include "Object.h"


Object::Object()
{
}


Object::~Object()
{
}

void Object::Draw()
{
	m_shader->Bind();
	m_texture->Bind();
	m_shader->SetAttribute("eye_pos", g_camera->GetPosition());
	m_shader->SetAttribute("eye_direction", g_camera->GetForward());
	m_shader->SetAttribute("angle", 30.f);
	m_shader->Update(m_transform);
	m_mesh->Draw();
}

void Object::Update(float dtime)
{
}
