#include "App.h"
#include "Scene.h"

App* g_app = nullptr;

App::App()
{
	assert(g_app == nullptr);
	g_app = this;
}

App::~App()
{
}

void App::Run()
{
	Display display(DISPLAY_WIDTH, DISPLAY_HEIGHT, "Tema3");

	Camera* camera = new Camera(Vector3f(5.0f, 2.f, 15.0f), 70.0f, (float)DISPLAY_WIDTH / (float)DISPLAY_HEIGHT, 0.1f, 100.0f);

	SDL_ShowCursor(0);

	Scene* scene = new Scene();
	SDL_Event e;
	m_isRunning = true;

	uint oldTime = SDL_GetTicks();

	float timer = 0;
	float frames = 0;
	display.SetMouseAtPosition(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2);
	while (m_isRunning)
	{

		while (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT || g_input->IsKeyDown(27))
				m_isRunning = false;
			g_input->ReadKey(e);
			display.SetMouseAtPosition(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2);
			g_scene->ComputeMouseMovementAngle();
		}

		display.Clear(0.2f, 0.2f, 0.2f, 1.0f);

		uint timeNow = SDL_GetTicks();
		int deltatime = timeNow - oldTime;
		oldTime = timeNow;

		timer += deltatime;
		if (timer > 1000)
		{
			//printf("%f\n", frames / (timer / 1000));
			timer = 0;
			frames = 0;
		}
		else
		{
			frames++;
		}

		scene->Update(float(deltatime));
		scene->Draw();

		display.SwapBuffers();
		SDL_Delay(1);
	}
}