#include "Scene.h"
#include "App.h"

Scene* g_scene = nullptr;

Scene::Scene()
{
	assert(g_scene == nullptr);
	g_scene = this;
	m_paused = false;
	m_input = new InputReader();

	Init();
}


Scene::~Scene()
{
}

void Scene::Init()
{
	m_floor = new Floor();
	m_panel = new Panel();
	m_panel->SetPosition(Vector3f(10.f, 3.f, 2.f));

	m_textContainer = new TextContainer(std::string("font2"), std::string("./res/textShader"), std::string("Tema 4 EGC!"), false);
	m_textContainer->GetPosition()->x += 1.5f;
	m_textContainer->GetPosition()->y += 1.2f;
	m_textContainer->GetPosition()->z += 3.2f;

	InitMenues();

	m_oldMousePos = Vector2f(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2);
}

void Scene::InitMenues()
{
	m_menuTexts.push_back(new TextContainer(std::string("font2"), std::string("./res/menuTextShader"), std::string("Resume"), true));
	m_menuTexts.back()->SetPosition(Vector3f(0, 3, 0));
	m_menuTexts.push_back(new TextContainer(std::string("font2"), std::string("./res/menuTextShader"), std::string("Options"), true));
	m_menuTexts.back()->SetPosition(Vector3f(0, 0, -100));
	m_menuTexts.push_back(new TextContainer(std::string("font2"), std::string("./res/menuTextShader"), std::string("Exit"), true));
	m_menuTexts.back()->SetPosition(Vector3f(0, -3, -100));
}

void Scene::TreatInput(float dtime)
{
	if (m_input->IsKeyPressed('`'))
	{
		m_paused = m_paused ? false : true;
	}

	if (m_input->IsKeyPressed('\t'))
	{
		if (m_input->IsInputModeOn())
			m_input->TurnInputOff();
		else
			m_input->TurnInputOn();
	}

	if (!m_input->IsInputModeOn())
	{
		if (m_input->IsKeyDown('a'))
			g_camera->MoveRight(dtime * 0.01f);
		if (m_input->IsKeyDown('d'))
			g_camera->MoveRight(-dtime * 0.01f);
		if (m_input->IsKeyDown('w'))
			g_camera->MoveForward(dtime * 0.01f);
		if (m_input->IsKeyDown('s'))
			g_camera->MoveForward(-dtime * 0.01f);
		if (m_input->IsKeyDown('r'))
			g_camera->SetPosition(Vector3f(4.0f, 4.0f, 0.0f));

		if (m_input->IsKeyDown('.'))
			m_textContainer->SetScale(*m_textContainer->GetScale() + 0.001f);
		if (m_input->IsKeyDown(','))
			m_textContainer->SetScale(*m_textContainer->GetScale() - 0.001f);

		if (m_input->IsKeyDown('['))
			m_textContainer->GetPosition()->x -= 0.01f;
		if (m_input->IsKeyDown(']'))
			m_textContainer->GetPosition()->x += 0.01f;


		if (m_input->IsKeyDown('m'))
			m_textContainer->SetMoving();
		if (m_input->IsKeyDown('n'))
			m_textContainer->SetNotMoving();
	}
}

void Scene::ComputeMouseMovementAngle()
{
	Vector2f newPos = m_input->GetNewMousePosition();

	Vector2f angle = Vector2f(0.2 * float(m_oldMousePos.y - newPos.y), 0.2 * float(m_oldMousePos.x - newPos.x));

	if (angle.x != 0)
		g_camera->RotateX(-angle.x);
	if (angle.y != 0)
		g_camera->RotateY(angle.y);
}

void Scene::Update(float dtime)
{
	TreatInput(dtime);
	m_floor->Update(dtime);
	m_panel->Update(dtime);
	m_textContainer->Update(dtime);

}

void Scene::Draw()
{
	if (m_paused)
		for (uint i = 0; i < m_menuTexts.size(); i++)
			m_menuTexts[i]->Draw();
	
	m_floor->Draw();
	m_panel->Draw();
	m_textContainer->Draw();

	
	
}

