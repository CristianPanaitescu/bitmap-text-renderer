#pragma once
#include "Object.h"
#include "FontDescriptor.h"

class Panel :
	public Object
{
public:
	Panel();
	~Panel();

private:

	void				InitVertices(std::vector<Vertex>& vertices);
	void				InitIndices(std::vector<uint>& indices);

	FontDescriptor*		m_fontDescriptor;
};

