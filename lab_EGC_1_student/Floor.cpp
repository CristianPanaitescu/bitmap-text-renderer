#include "Floor.h"

Floor::Floor()
{
	std::vector<Vertex> vertices;
	std::vector<uint> indices;

	InitVertices(vertices);
	InitIndices(indices);

	m_mesh = new Mesh(vertices, vertices.size(), indices, indices.size(), true);
	m_shader = new Shader("./res/basicShader");
	m_texture = new Texture("./res/stone_floor.png");
	m_transform = new Transform();
}


Floor::~Floor()
{
}

void Floor::InitVertices(std::vector<Vertex>& vertices)
{
	vertices.push_back(Vertex(Vector3f(0, 0, 0), glm::vec2(0, 0), Vector3f(0, 1, 0)));
	vertices.push_back(Vertex(Vector3f(0, 0, 20), glm::vec2(0, 1), Vector3f(0, 1, 0)));
	vertices.push_back(Vertex(Vector3f(20, 0, 20), glm::vec2(1, 1), Vector3f(0, 1, 0)));
	vertices.push_back(Vertex(Vector3f(20, 0, 0), glm::vec2(1, 0), Vector3f(0, 1, 0)));
}

void Floor::InitIndices(std::vector<uint>& indices)
{
		indices.push_back(0);
		indices.push_back(1);
		indices.push_back(2);
		indices.push_back(0);
		indices.push_back(2);
		indices.push_back(3);
}