#pragma once
#include "Utils.h"

struct CharDescriptor
{
	Vector2f i1;
	Vector2f i2;
	Vector2f i3;
	Vector2f i4;
	Vector2f charSize;
	bool	 valid;
};

class FontDescriptor
{
public:
	FontDescriptor(std::string fontName);
	~FontDescriptor();

	CharDescriptor			GetCharDescription(char c);

private:

	void Init();

	std::string				m_fontName;
	int						m_charCount;
	int						m_charWidth;
	int						m_charHeight;
	int						m_charsPerLine;
	int						m_textureWidth;
	int						m_textureHeight;

	std::string				m_fontChars;
};

