#include "Panel.h"


Panel::Panel()
{
	std::vector<Vertex> vertices;
	std::vector<uint> indices;

	InitVertices(vertices);
	InitIndices(indices);

	m_mesh = new Mesh(vertices, vertices.size(), indices, indices.size(), true);
	m_shader = new Shader("./res/basicShader");
	m_texture = new Texture("./res/wall.jpg");
	m_transform = new Transform();
}


Panel::~Panel()
{
}

void Panel::InitVertices(std::vector<Vertex>& vertices)
{
	vertices.push_back(Vertex(Vector3f(-10, -2, 1), glm::vec2(0, 0), Vector3f(0, 0, 1)));
	vertices.push_back(Vertex(Vector3f(10, -2, 1), glm::vec2(0, 1), Vector3f(0, 0, 1)));
	vertices.push_back(Vertex(Vector3f(10, 2, 1), glm::vec2(1, 1), Vector3f(0, 0, 1)));
	vertices.push_back(Vertex(Vector3f(-10, 2, 1), glm::vec2(1, 0), Vector3f(0, 0, 1)));

	vertices.push_back(Vertex(Vector3f(10, -2, 1), glm::vec2(0, 0), Vector3f(1, 0, 0)));
	vertices.push_back(Vertex(Vector3f(10, -2, -1), glm::vec2(0, 1), Vector3f(1, 0, 0)));
	vertices.push_back(Vertex(Vector3f(10, 2, -1), glm::vec2(1, 1), Vector3f(1, 0, 0)));
	vertices.push_back(Vertex(Vector3f(10, 2, 1), glm::vec2(1, 0), Vector3f(1, 0, 0)));

	vertices.push_back(Vertex(Vector3f(10, -2, -1), glm::vec2(0, 0), Vector3f(0, 0, -1)));
	vertices.push_back(Vertex(Vector3f(-10, -2, -1), glm::vec2(0, 1), Vector3f(0, 0, -1)));
	vertices.push_back(Vertex(Vector3f(-10, 2, -1), glm::vec2(1, 1), Vector3f(0, 0, -1)));
	vertices.push_back(Vertex(Vector3f(10, 2, -1), glm::vec2(1, 0), Vector3f(0, 0, -1)));

	vertices.push_back(Vertex(Vector3f(-10, -2, -1), glm::vec2(0, 0), Vector3f(-1, 0, 0)));
	vertices.push_back(Vertex(Vector3f(-10, -2, 1), glm::vec2(0, 1), Vector3f(-1, 0, 0)));
	vertices.push_back(Vertex(Vector3f(-10, 2, 1), glm::vec2(1, 1), Vector3f(-1, 0, 0)));
	vertices.push_back(Vertex(Vector3f(-10, 2, -1), glm::vec2(1, 0), Vector3f(-1, 0, 0)));

	vertices.push_back(Vertex(Vector3f(-10, 2, 1), glm::vec2(0.4, 0.4), Vector3f(0, 1, 0)));
	vertices.push_back(Vertex(Vector3f(10, 2, 1), glm::vec2(0.4, 0.6), Vector3f(0, 1, 0)));
	vertices.push_back(Vertex(Vector3f(10, 2, -1), glm::vec2(0.6, 0.6), Vector3f(0, 1, 0)));
	vertices.push_back(Vertex(Vector3f(-10, 2, -1), glm::vec2(0.6, 0.4), Vector3f(0, 1, 0)));

	vertices.push_back(Vertex(Vector3f(-10, -2, 1), glm::vec2(0.4, 0.4), Vector3f(0, -1, 0)));
	vertices.push_back(Vertex(Vector3f(-10, -2, -1), glm::vec2(0.4, 0.6), Vector3f(0, -1, 0)));
	vertices.push_back(Vertex(Vector3f(10, -2, -1), glm::vec2(0.6, 0.6), Vector3f(0, -1, 0)));
	vertices.push_back(Vertex(Vector3f(10, -2, 1), glm::vec2(0.6, 0.4), Vector3f(0, -1, 0)));
}

void Panel::InitIndices(std::vector<uint>& indices)
{
	for (int i = 0; i < 6; i++)
	{
		indices.push_back(4 * i + 0);
		indices.push_back(4 * i + 1);
		indices.push_back(4 * i + 2);
		indices.push_back(4 * i + 0);
		indices.push_back(4 * i + 2);
		indices.push_back(4 * i + 3);
	}
}