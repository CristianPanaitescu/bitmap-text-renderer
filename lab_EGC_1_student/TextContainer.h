#pragma once
#include "Object.h"
#include "FontDescriptor.h"

class TextContainer :
	public Object
{
public:
	TextContainer(const std::string& texture, const std::string& shader, const std::string& text, bool isMenu);
	~TextContainer();

	virtual void		Draw();
	virtual void		Update(float dtime);

	void				DeleteLastCharacter();
	void				AddCharacter(char c);

	void				SetMoving() { m_isMoving = true; };
	void				SetNotMoving() { m_isMoving = false; };

	void				SetText(std::string text) { m_text = text; }

	uint				m_textSize;

private:

	void				InitVertices(std::vector<Vertex>& vertices);
	void				InitIndices(std::vector<uint>& indices);

	Mesh*				CreateMesh();

	FontDescriptor*		m_fontDescriptor;
	std::string			m_text;
	bool				m_isMoving;
	bool				m_isMenu;
	
};

