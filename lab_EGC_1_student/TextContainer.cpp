#include "TextContainer.h"


TextContainer::TextContainer(const std::string& texture, const std::string& shader, const std::string& text, bool isMenu)
{
	m_text = text;
	
	m_isMenu = isMenu;

	if (m_isMenu)
		m_textSize = 5;
	else
		m_textSize = 4;

	m_fontDescriptor = new FontDescriptor(texture);
	m_isMoving = false;

	m_mesh = CreateMesh();
	m_shader = new Shader(shader);
	m_texture = new Texture(std::string("./res/") + texture + std::string(".png"));
	m_transform = new Transform();
}


TextContainer::~TextContainer()
{
}

void TextContainer::Draw()
{
	if (m_text.size() == 0)
	{
		return;
	}

	m_shader->Bind();
	m_texture->Bind();
	m_shader->SetAttribute("left_margin", 1.f);
	m_shader->SetAttribute("right_margin", 19.f);
	m_shader->SetAttribute("eye_position", g_camera->GetPosition());
	m_shader->SetAttribute("eye_direction", g_camera->GetForward());
	m_shader->SetAttribute("angle", 20.f);
	if (!m_isMenu)
	{
		m_shader->Update(m_transform);
	}
	else
	{
		m_shader->UpdateOrtho(m_transform);
	}
	m_mesh->Draw();
}

void TextContainer::Update(float dtime)
{
	if (!m_isMenu)
	{
		const float left = 1.f;
		const float right = 20.f;
		if (m_isMoving)
		{
			GetPosition()->x -= dtime * 0.01f;
			if (left > GetScale()->x * (GetPosition()->x + m_fontDescriptor->GetCharDescription(' ').charSize.x * m_textSize * m_text.size()))
				GetPosition()->x = right;
		}
	}
}

void TextContainer::DeleteLastCharacter()
{
	if (m_text.size() >= 1)
	{
		m_text.erase(m_text.begin() + m_text.size() - 1);
	}
	m_mesh = CreateMesh();
}

void TextContainer::AddCharacter(char c)
{
	if (c != 8) // not backspace
	{
		if (m_fontDescriptor->GetCharDescription(c).valid)
		{
			m_text.append(&c);
		}
	}
	else
	{
		DeleteLastCharacter();
	}
	m_mesh = CreateMesh();
}

Mesh* TextContainer::CreateMesh()
{
	std::vector<Vertex> vertices;
	std::vector<uint> indices;

	InitVertices(vertices);
	InitIndices(indices);

	if (m_text.size() != 0)
		return new Mesh(vertices, vertices.size(), indices, indices.size(), true);
	else
		return nullptr;
}

void TextContainer::InitVertices(std::vector<Vertex>& vertices)
{
	for (uint i = 0; i < m_text.size(); i++)
	{
		CharDescriptor charDescr = m_fontDescriptor->GetCharDescription(m_text.at(i));
		if (charDescr.valid == false)
		{
			continue;
		}

		Vector2f charSize = Vector2f(charDescr.charSize.x * m_textSize, charDescr.charSize.y * m_textSize);
		Vector3f normal = m_isMenu ? Vector3f(0, 0, 1) : Vector3f(0, 0, 1);
		vertices.push_back(Vertex(Vector3f(i * charSize.x, 0, 0), charDescr.i2, normal));
		vertices.push_back(Vertex(Vector3f((i + 1) * charSize.x, 0, 0), charDescr.i3, normal));
		vertices.push_back(Vertex(Vector3f((i + 1) * charSize.x, charSize.y, 0), charDescr.i4, normal));
		vertices.push_back(Vertex(Vector3f(i * charSize.x, charSize.y, 0), charDescr.i1, normal));
	}
}

void TextContainer::InitIndices(std::vector<uint>& indices)
{
	for (uint i = 0; i < m_text.size(); i++)
	{
		indices.push_back(4 * i + 0);
		indices.push_back(4 * i + 1);
		indices.push_back(4 * i + 2);
		indices.push_back(4 * i + 0);
		indices.push_back(4 * i + 2);
		indices.push_back(4 * i + 3);

		indices.push_back(4 * i + 2);
		indices.push_back(4 * i + 1);
		indices.push_back(4 * i + 0);
		indices.push_back(4 * i + 3);
		indices.push_back(4 * i + 2);
		indices.push_back(4 * i + 0);
	}
}
