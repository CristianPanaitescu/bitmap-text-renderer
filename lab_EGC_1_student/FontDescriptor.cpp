#include "FontDescriptor.h"
#include <fstream>
#include <string>

FontDescriptor::FontDescriptor(std::string fontName)
{
	m_fontName = fontName;
	Init();
}


FontDescriptor::~FontDescriptor()
{
}

CharDescriptor FontDescriptor::GetCharDescription(char c)
{
	CharDescriptor descr;

	for (uint i = 0; i < m_fontChars.size(); i++)
	{
		if (m_fontChars.at(i) == c)
		{
			int line = i / m_charsPerLine;
			int column = i % m_charsPerLine;
			descr.i1 = Vector2f(float(line * m_charHeight) / m_textureHeight, 1 - float(column * m_charWidth) / m_textureWidth);
			descr.i2 = Vector2f(float((line + 1) * m_charHeight) / m_textureHeight, 1 - float(column * m_charWidth) / m_textureWidth);
			descr.i3 = Vector2f(float((line + 1) * m_charHeight) / m_textureHeight, 1 - float((column + 1) * m_charWidth) / m_textureWidth);
			descr.i4 = Vector2f(float(line * m_charHeight) / m_textureHeight, 1 - float((column + 1) * m_charWidth) / m_textureWidth);
			descr.charSize = glm::normalize(Vector2f(m_charWidth, m_charHeight));
			descr.valid = true;
			return descr;
		}
	}

	descr.valid = false;
	return descr;
}

void FontDescriptor::Init()
{
	std::ifstream myfile("./res/" + m_fontName + ".fnt");
	if (myfile.is_open())
	{
		myfile >> m_charCount >> m_charWidth >> m_charHeight >> m_charsPerLine >> m_textureWidth >> m_textureHeight;
		char* str = new char(m_charCount);
		std::getline(myfile, m_fontChars);
		std::getline(myfile, m_fontChars);
		myfile.close();
	}
}