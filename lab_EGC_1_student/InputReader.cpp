#include "InputReader.h"
#include "Scene.h"

InputReader* g_input = nullptr;

InputReader::InputReader()
{
	assert(g_input == nullptr);
	g_input = this;
	m_isInputModeOn = false;
	RefreshKeys();
}

InputReader::~InputReader()
{
}

bool InputReader::IsMousePressed()
{ 
	return m_mousePressed;
	m_mousePressed = false;
}

void InputReader::ReadKey(SDL_Event& event)
{
	if (event.type == SDL_KEYDOWN && event.key.keysym.sym < KEYS_NO)
	{
		if (event.key.keysym.sym != 27 && IsInputModeOn())
			g_scene->m_textContainer->AddCharacter(event.key.keysym.sym);
		keys[event.key.keysym.sym] = true;
		keysDown[event.key.keysym.sym] = true;
	}
		
	else if (event.type == SDL_KEYUP && event.key.keysym.sym < KEYS_NO)
	{
		keys[event.key.keysym.sym] = false;
		keysDown[event.key.keysym.sym] = false;
	}
		

	if (event.button.button == SDL_BUTTON_LEFT && event.type == SDL_MOUSEBUTTONDOWN)
		m_mousePressed = true;
	else if (event.button.button == SDL_BUTTON_LEFT && event.type == SDL_MOUSEBUTTONUP)
		m_mousePressed = false;
}

bool InputReader::IsKeyPressed(char key) 
{
	if (keysDown[key])
	{
		keysDown[key] = false;
		return true;
	}
	return false;
}

void InputReader::RefreshKeys()
{
	for (int i = 0; i < KEYS_NO; i++)
	{
		keys[i] = false;
		keysDown[i] = false;
	}
		
	m_mousePressed = false;
}

Vector2f InputReader::GetNewMousePosition()
{
	int x, y;
	SDL_GetMouseState(&x, &y);

	return Vector2f(float(x), float(y));
}