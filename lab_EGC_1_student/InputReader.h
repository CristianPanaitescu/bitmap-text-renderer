#pragma once
#include "Utils.h"

#define KEYS_NO 6000

class InputReader
{
public:
	InputReader();
	~InputReader();

	void		ReadKey(SDL_Event& event);

	bool		IsKeyDown(char key) { return keys[key]; }
	bool		IsKeyPressed(char key); 
	bool		IsMousePressed();
	Vector2f	GetNewMousePosition();

	void		TurnInputOn() { m_isInputModeOn = true; }
	void		TurnInputOff() {	m_isInputModeOn = false; }
	bool		IsInputModeOn() { return m_isInputModeOn; }
	
	void		TurnCapsLockOn() { m_isCapsLockOn = true; }
	void		TurnCapsLockOff() { m_isCapsLockOn = false; }
	bool		IsCapsLockOn() { return m_isCapsLockOn; }


private:
	
	void		RefreshKeys();

	bool		keys[KEYS_NO];
	bool		keysDown[KEYS_NO];
	bool		m_mousePressed;
	bool		m_isInputModeOn;
	bool		m_isCapsLockOn;
};

extern InputReader* g_input;

