#pragma once

#include "Utils.h"
#include "transform.h"

class Shader
{
public:
	Shader(const std::string& fileName);

	void 				Bind();
	void 				Update(const Transform* transform);
	void 				UpdateOrtho(const Transform* transform);
	void 				UpdateLightPosition(Vector3f pos);
	void				UpdateEyePosition(Vector3f pos);
	void				SetAttribute(std::string attr, float val);
	void				SetAttribute(std::string attr, std::vector<float> val);
	void 				SetAttribute(std::string attr, Vector2f val);
	void 				SetAttribute(std::string attr, std::vector<Vector2f> val);
	void 				SetAttribute(std::string attr, Vector3f val);

	virtual ~Shader();
protected:
private:

	static const unsigned int NUM_UNIFORMS = 6;
	static const unsigned int NUM_SHADERS = 2;

	std::string			LoadShader(const std::string& fileName);
	void				CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage);
	GLuint				CreateShader(const std::string& text, unsigned int type);

	

	GLuint				m_program;
	GLuint				m_shaders[NUM_SHADERS];
	GLuint				m_uniforms[NUM_UNIFORMS];

	Vector3f			m_lightPosition;
	Vector3f			m_eyePosition;
};

