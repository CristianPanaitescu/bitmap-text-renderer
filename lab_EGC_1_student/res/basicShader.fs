#version 330

#define PI 3.1415926535

varying vec3 normal0;
varying vec2 texCoord0;
varying vec3 position0;

uniform sampler2D sampler;
uniform vec3 eye_pos;
uniform vec3 eye_direction;
uniform float angle;


void main()
{
    vec4 texture = texture2D(sampler, texCoord0);
    //vec3 eye_position = a;
    vec4 light;

    vec3 ld = position0 - eye_pos;
    vec3 sd = eye_direction;

    float ll = length(ld);
	float sl = length(sd);

	//ld = normalize(ld);
	//sd = normalize(sd);

    float theta = acos(dot(ld, sd) / (ll * sl));


	light = vec4(1,0.8,0.4,1) * 2/sqrt(ll) * 1/theta;
	gl_FragColor = texture * light; 
    
}
