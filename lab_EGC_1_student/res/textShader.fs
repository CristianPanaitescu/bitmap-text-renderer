#version 330

varying vec3 normal0;
varying vec2 texCoord0;
varying vec3 position0;

uniform sampler2D sampler;
uniform float left_margin;
uniform float right_margin;

void main()
{
    if(position0.x < left_margin || position0.x > right_margin)
        discard;
    else if(position0.x - left_margin < 1)
	    gl_FragColor = texture2D(sampler, texCoord0) * vec4(1, 1, 1, position0.x - left_margin );
    else if( - position0.x + right_margin < 1)
	    gl_FragColor = texture2D(sampler, texCoord0) * vec4(1, 1, 1, - position0.x + right_margin );
    
    else
        gl_FragColor = texture2D(sampler, texCoord0);
}
