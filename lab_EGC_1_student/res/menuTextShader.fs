#version 330

varying vec3 normal0;
varying vec2 texCoord0;
varying vec3 position0;

uniform sampler2D sampler;
uniform float selected;

void main()
{
    vec4 texture = texture2D(sampler, texCoord0);
    if(texture.a == 0)
        discard;
	gl_FragColor = texture;
}
