#version 330

#define PI 3.1415926535

attribute vec3 position;
attribute vec2 texCoord;
attribute vec3 normal;

varying vec2 texCoord0;
	
uniform float size;
uniform float waves_no;
uniform float S;
uniform float material_kd;
uniform float material_ks;

uniform float amps[10];
uniform vec2  dirs[10];
uniform float Ls[10];
uniform float dirTypes[10];
uniform vec2  centers[10];
uniform float maxRanges[10];
uniform float T[10];
 
uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

uniform vec3 light_position;
uniform vec3 eye_position;

uniform mat4 Normal;
out float light0;

float computeY(vec2 crtPos)
{
	int i = 0;
	float y = 0;
	for(i = 0 ; i < waves_no ; i++)
	{
		float value;
		float amp;
		float speed;
		if(dirTypes[i] == 0)
		{
			vec2 dir = (centers[i] - crtPos);
			value = -distance(dir, crtPos);
			if(distance(crtPos * 2, centers[i]) > maxRanges[i])
				continue;
			speed = S;
		}

		else
		{
			vec2 dir = dirs[i];
			value = dot(dir, crtPos);
			speed = S / 2;
		}
		float paranteza = (sin(value * 2 * PI / Ls[i] + T[i] * speed * 2 * PI / Ls[i]) + 1) / 2;
		y += 2 * amps[i] * (paranteza * paranteza);
	}
	return y;
}

void main()
{
	vec2 v0 = vec2(position.x, position.z); 
	vec2 v1 = vec2(position.x + size, position.z); 
	vec2 v2 = vec2(position.x, position.z + size); 
	vec2 v3 = vec2(position.x - size, position.z); 
	vec2 v4 = vec2(position.x, position.z - size); 

	float y0 = computeY(v0);
	float y1 = computeY(v1);
	float y2 = computeY(v2);
	float y3 = computeY(v3);
	float y4 = computeY(v4);

	vec3 crtVert = vec3(v0.x, y0, v0.y);
	vec3 l1 = vec3(v1.x, y1, v1.y) - crtVert;
	vec3 l2 = vec3(v2.x, y2, v2.y) - crtVert;
	vec3 l3 = vec3(v3.x, y3, v3.y) - crtVert;
	vec3 l4 = vec3(v4.x, y4, v4.y) - crtVert;

	vec3 norm = - normalize((cross(l1, l2) + cross(l2, l3) + cross(l3, l4) + cross(l1, l4)) / 4);

	vec3 world_pos = (model_matrix * vec4(crtVert, 1)).xyz;
	vec3 world_normal = normalize( mat3(model_matrix) * norm);
	vec3 L = normalize(light_position - world_pos);
	vec3 V = normalize(eye_position - world_pos);

	float pr = (dot(world_normal, L) > 0) ? 1 : 0;

	vec3 H = normalize(L + V);

	float D = material_kd * max(dot(norm, L), 0);
	float S = material_ks * pr * pow((max(dot(norm, H), 0)), 10);

	light0 = S + D;

	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(crtVert, 1.0);
	texCoord0 = texCoord;
}
