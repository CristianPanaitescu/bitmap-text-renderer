#pragma once
#include "Object.h"
class Floor :
	public Object
{
public:
	Floor();
	~Floor();

private:

	void				InitVertices(std::vector<Vertex>& vertices);
	void				InitIndices(std::vector<uint>& indices);
};

